import 'package:flutter/material.dart';
import 'package:qr_reader/widgets/scan_list_tile.dart';

class MapsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScanListTile(type: 'geo');
  }
}

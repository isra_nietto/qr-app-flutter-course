import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qr_reader/providers/db_provider.dart';
import 'package:qr_reader/providers/scans_provider.dart';
import 'package:qr_reader/providers/ui_provider.dart';

import 'package:qr_reader/pages/address_page.dart';
import 'package:qr_reader/pages/maps_page.dart';
import 'package:qr_reader/widgets/custom_navigatorbar.dart';
import 'package:qr_reader/widgets/scan_button_navigator.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Historial'),
          // elevation: 0,
          actions: [
            IconButton(
                icon: Icon(Icons.delete_forever),
                onPressed: () {
                  final scanListProvider =
                      Provider.of<ScansProvider>(context, listen: false);
                  scanListProvider.deleteAll();
                })
          ],
        ),
        body: _HomePageBody(),
        bottomNavigationBar: CustomNavigatorBar(),
        floatingActionButton: ScanButtonNavigator(),
        floatingActionButtonLocation:
            FloatingActionButtonLocation.centerDocked);
  }
}

class _HomePageBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final uiProvider = Provider.of<UiProvider>(context);

    final currentIndex = uiProvider.selectedMenuOpt;

    // Usar el ScanProvider
    final scanlist = Provider.of<ScansProvider>(context, listen: false);

    switch (currentIndex) {
      case 0:
        scanlist.loadScansByType('geo');
        return MapsPage();

      case 1:
        scanlist.loadScansByType('http');
        return AddressPage();

      default:
        return MapsPage();
    }
  }
}

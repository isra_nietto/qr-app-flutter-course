import 'package:flutter/material.dart';
import 'package:qr_reader/widgets/scan_list_tile.dart';

class AddressPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScanListTile(type: 'http');
  }
}

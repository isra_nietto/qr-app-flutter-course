import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qr_reader/providers/scans_provider.dart';
import 'package:qr_reader/utils/utils.dart';

class ScanListTile extends StatelessWidget {
  final String type;
  const ScanListTile({@required this.type});

  @override
  Widget build(BuildContext context) {
    final scanListProvider = Provider.of<ScansProvider>(context);
    final scans = scanListProvider.scans;

    return ListView.builder(
        itemCount: scans.length,
        itemBuilder: (BuildContext context, int index) {
          return Dismissible(
            key: UniqueKey(), // se encarga de generar un key unico por nosotros
            background: Container(
              color: Colors.red,
            ),
            onDismissed: (DismissDirection direction) {
              Provider.of<ScansProvider>(context, listen: false)
                  .deleteScanById(scans[index].id);
            },
            child: ListTile(
              leading: Icon(this.type == 'http' ? Icons.home : Icons.map,
                  color: Theme.of(context).primaryColor),
              title: Text(scans[index].value),
              subtitle: Text('ID: ${scans[index].id}'),
              trailing: Icon(Icons.keyboard_arrow_right, color: Colors.grey),
              onTap: () => launchURL(context, scans[index]),
            ),
          );
        });
  }
}

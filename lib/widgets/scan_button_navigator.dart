import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:provider/provider.dart';
import 'package:qr_reader/providers/scans_provider.dart';
import 'package:qr_reader/utils/utils.dart';

class ScanButtonNavigator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
        elevation: 2,
        child: Icon(Icons.filter_center_focus),
        onPressed: () async {
          // String barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
          //     '#3D8BEF', 'Cancelar', false, ScanMode.QR);
          // final barcodeScanRes = 'https://google.com';
          final barcodeScanRes = 'geo:28.685591,-106.041184';
          if (barcodeScanRes == '-1') {
            return;
          }

          final scanListProvider =
              Provider.of<ScansProvider>(context, listen: false);
          final newScan = await scanListProvider.newServiceScan(barcodeScanRes);
          // scanListProvider.newServiceScan('geo:243.34,345.23');

          launchURL(context, newScan);
        });
  }
}

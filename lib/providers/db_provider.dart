import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

import 'package:qr_reader/models/scan_model.dart';
export 'package:qr_reader/models/scan_model.dart';

class DBProvider {
  static Database _database;

  //Se utiliza Singleton para que solamente se use una instancia
  // ._() es un constructor privado de sinbleton
  static final DBProvider db = DBProvider._();
  DBProvider._();

  Future<Database> get database async {
    if (_database != null) return _database;

    _database = await initDB();

    return _database;
  }

  Future<Database> initDB() async {
    // Path don donde almacenaremos la base de datos

    Directory documetsDirectory = await getApplicationDocumentsDirectory();
    final path = join(documetsDirectory.path, 'ScansDB.db');
    print(path);

    //Create DB
    return await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      await db.execute(''' 
        CREATE TABLE scans(
          id INTEGER PRIMARY KEY,
          type TEXT,
          value TEXT
        ) 
      ''');
    });
  }

  Future<int> newScanRaw(ScanModel newScan) async {
    //verificar la base de datos
    final id = newScan.id;
    final type = newScan.type;
    final value = newScan.value;

    final db = await database;

    final res = await db.rawInsert('''
      INSERT INTO scans(id, type, value)
        VALUES( $id, $type, $value)
    ''');
    return res;
  }

  Future<int> newScan(ScanModel newScan) async {
    final db = await database;
    final res = await db.insert('scans', newScan.toJson());

    // res Es el ultimo Id insertado
    return res;
  }

  Future<ScanModel> getScanById(int id) async {
    final db = await database;
    final res = await db.query('scans', where: 'id = ?', whereArgs: [id]);

    return res.isNotEmpty ? ScanModel.fromJson(res.first) : null;
    // se llama
    // DBProvider.db.getScanById(2).then((scan) => print(scan.value));
  }

  Future<List<ScanModel>> getAllScans() async {
    final db = await database;
    final res = await db.query('scans');

    return res.isNotEmpty
        ? res.map((scan) => ScanModel.fromJson(scan)).toList()
        : [];
  }

  Future<List<ScanModel>> getScansByType(String type) async {
    final db = await database;
    final res = await db.rawQuery('''
      SELECT * FROM scans WHERE type = '$type'
    ''');

    return res.isNotEmpty
        ? res.map((scan) => ScanModel.fromJson(scan)).toList()
        : [];
  }

  Future<int> updateScan(ScanModel newScan) async {
    final db = await database;
    // si no ponemos where se actuliza todas las filas de la tabla
    final res = await db.update('scans', newScan.toJson(),
        where: 'id = ?', whereArgs: [newScan.id]);

    // res Es el ultimo Id actulizado
    return res;
  }

  Future<int> deleteScan(int id) async {
    final db = await database;
    final res = await db.delete('scans', where: 'id = ?', whereArgs: [id]);
    return res;
  }

  Future<int> deleteAllScanSimple() async {
    final db = await database;
    final res = await db.delete('scans');
    return res;
  }

  Future<int> deleteAllScan() async {
    final db = await database;
    // se puede generar un query mas avanzado
    final res = await db.rawDelete(''' 
      DELETE FROM scans
    ''');
    return res;
  }
}
